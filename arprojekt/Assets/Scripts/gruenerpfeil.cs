﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gruenerpfeil : MonoBehaviour
{
    private GameObject balken;
    private MeshRenderer rendererbalken;
    public Animator pointpfeil;

    // Start is called before the first frame update
    void Start()
    {
        balken = GameObject.Find("gruenerPfeil");
        rendererbalken = balken.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rendererbalken.enabled)
        {
            pointpfeil.SetBool("isrendered", true);
        }
        if (!rendererbalken.enabled)
        {
            pointpfeil.SetBool("isrendered", false);
        }
    }
}
