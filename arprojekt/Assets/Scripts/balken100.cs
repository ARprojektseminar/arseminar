﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balken100 : MonoBehaviour
{
    private GameObject balken;
    private MeshRenderer rendererbalken;
    public Animator animatorbalken;

    // Start is called before the first frame update
    void Start()
    {
        balken = GameObject.Find("balken100");
        rendererbalken = balken.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(rendererbalken.enabled)
        {
            animatorbalken.SetBool("isrendered", true);
        }
        if(!rendererbalken.enabled)
        {
            animatorbalken.SetBool("isrendered", false);
        }
    }
}
