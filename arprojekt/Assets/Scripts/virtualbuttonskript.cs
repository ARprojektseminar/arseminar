﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.SceneManagement;

public class virtualbuttonskript : MonoBehaviour, IVirtualButtonEventHandler
{
    public GameObject vbBtnObj;

    void Start()
    {
        vbBtnObj = GameObject.Find("VirtualButton100");
        vbBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        SceneManager.LoadScene("Step_11");
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {

    }

}
